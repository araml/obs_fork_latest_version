#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QSqlDatabase>
#include <QWebEngineView>
#include <QSqlTableModel>
#include <QMessageBox>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlRecord>
#include <QSqlDatabase>
#include <QFile>
#include <QMediaPlayer>
#include <QDialogButtonBox>
#include <math.h>
#include "setevent.h"
#include "tabledelegate.h"
#include "tablemodelparticipants.h"

#include <QWidget>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QString>
// PDE:
#include <thread>
#include <condition_variable>
#include <atomic>
#include <QTextStream>
#include <QSerialPort>
#include <QSerialPortInfo>

//PDE SERIAL
#include <string>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTextStream>
// ENDPDE


namespace Ui {
class MainWindow;
}

class QSqlTableModel;
enum State { INITIAL, COMP_GO, COMP_PAUSE, COMP_RESUME, COMP_START, COMP_END };

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void update_go_button_title();
    void resetAll();

public slots:
    void on_retired_clicked();
    void go_set(int index);
    void set_table(Setting *setting);
    void reset_obstacles();
    void reset_counters();
    void update_event(Setting *setting);
    void update_bd_obstacles(QList<QList<int> > data);
    void update_obstacles(QList<QList<int> > data);
    void update_faults();
    void update_countdown();
    void update_results();
    void playMp3(QString strQResourceFile);
    void update_roundNum(int round);
    void start_recording();
    void stop_recording();
    QString rename_video();
    void send_video_fb();

    void on_mediaPlayer_mediaStatusChanged(QMediaPlayer::MediaStatus status);

    void on_go_button_pressed();

    void on_start_button_clicked();

    void on_end_button_clicked();

    void on_pause_clicked();

    void on_eliminated_clicked();

    void on_bell_clicked();

    void on_timeAllowedSpin_valueChanged(double arg1);

    void fourFaults_clicked();

    void on_not_participate_clicked();

    void replyFinished(QNetworkReply *repy);

    void alertbarrier();


private:
    Ui::MainWindow* ui;
    State state;
    TableModelParticipants* mModel;
//    QSqlTableModel* resultsModel;
    QSqlDatabase* mDatabase;
    QSqlQuery* argEventQuery;
    TableDelegate* tableDelegate;
    QList <QWidget*> obstaclesButtons;
    QTimer* timer;
    QString results_htmlTemplate;
    QString results_tableTemplate;
    QString event_id;
    QString event_N;

    QString c_country,c_event_place,c_place_code, c_height, oldvideoName, newvideoName, page;

    QMediaPlayer mMediaPlayer;
    double mElapseTime;
    double mFaultAsTime;
    double mTimeAllow;
    double mTimeLimit;
    bool mAgainstClock;
    char mTableCompl;
    bool bBelRingging;
    bool firsttime;
    int roundNum=1;
    int jumpOffNum=0;
    int onupload;
    int bs;

//    QWebEngineProfile* defaultProfile;
    QNetworkAccessManager *m_manager;
    QNetworkReply *replay;
    QString m_fileName;
    QFile *m_file;
    // PDE SERIAL


private:
            QSerialPort port;
//            std::chrono::steady_clock::time_point prev_clock;
            bool start_mode{ true };
            std::string start{ "Sta" };
            std::string stop{ "Sto" };
            size_t idx{ 0 };

       //EPDE SERIAL

public slots:
                void handle_read();
    void closeEvent(QCloseEvent *event);
private slots:
//    void on_ir_start_clicked();
//    void on_ir_end_clicked();
//    void record(OBSScene scene, int monitor);
signals:
    void close_app();
    void ir_start();
    void ir_end();
};

#endif // MAINWINDOW_H
