#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QMutex>
#include <QHash>
#include <QSqlDatabase>

class QThread;

class DatabaseManager
{
    public:
        static QSqlDatabase database(const QString& connectionName = QLatin1String(QSqlDatabase::defaultConnection));
    private:
        static QMutex s_databaseMutex;
        static QHash<QThread*, QHash<QString,QSqlDatabase>> s_instances;
};

#endif // DATABASEMANAGER_H
