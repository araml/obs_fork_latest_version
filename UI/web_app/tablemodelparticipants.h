#ifndef TABLEMODELPARTICIPANTS_H
#define TABLEMODELPARTICIPANTS_H
#include <QMimeData>
#include <QSqlTableModel>
#include <QString>

class TableModelParticipants : public QSqlTableModel
{
public:
    TableModelParticipants();
    explicit TableModelParticipants(QObject *parent = Q_NULLPTR, QSqlDatabase db = QSqlDatabase());

    Qt::ItemFlags flags(const QModelIndex &index) const;

    Qt::DropActions supportedDropActions() const;

    QMimeData *mimeData(const QModelIndexList &indexes) const;

    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);
};

#endif // TABLEMODELPARTICIPANTS_H
