#include "setevent.h"
#include "ui_setevent.h"
#include <QIcon>

SetEvent::SetEvent(QVariant narticle, QWidget *parent) :
    QWidget(parent, Qt::Dialog),
    ui(new Ui::SetEvent)
{
    // Window stuff
    ui->setupUi(this);

    // Add items to the placing_combo comboboxes for 1st round
    ui->placing_combo->addItem("According to the penalties");
    ui->placing_combo->addItem("According to the penalties and time");
    ui->placing_combo->setCurrentIndex(1);

    setAttribute(Qt::WA_DeleteOnClose);

    // For each row of round/jo widgets, configure them properly.
    QList<QWidget *> fieldWidgets = ui->fields_widget->findChildren<QWidget *>(QRegExp(".*_widget"));
    for (int i = 0; i < fieldWidgets.length(); i++) {
        QComboBox *speedCombo = fieldWidgets[i]->findChildren<QComboBox *>(QRegExp(".*Speed_combo"))[0];
        QLineEdit *timeALine = fieldWidgets[i]->findChildren<QLineEdit *>(QRegExp(".*TimeA_line"))[0];
        QLineEdit *timeLLine = fieldWidgets[i]->findChildren<QLineEdit *>(QRegExp(".*TimeL_line"))[0];
        QLineEdit *distanceLine = fieldWidgets[i]->findChildren<QLineEdit *>(QRegExp(".*Distance_line"))[0];
        QComboBox *tableCombo = fieldWidgets[i]->findChildren<QComboBox *>(QRegExp(".*Table_combo"))[0];

        connect(speedCombo, &QComboBox::currentTextChanged, [=] {
            updateTimes(speedCombo, distanceLine, timeALine, timeLLine);
        });
        connect(distanceLine, &QLineEdit::textChanged, [=] {
            updateTimes(speedCombo, distanceLine, timeALine, timeLLine);
        });

        // Add items to the Speed comboboxes
        speedCombo->addItem("300");
        speedCombo->addItem("325");
        speedCombo->addItem("350");
        speedCombo->addItem("375");
        speedCombo->addItem("400");

        // Add items to the Table comboboxes
        tableCombo->addItem("A");
        tableCombo->addItem("C");

        // By default, set the speed = 300
        speedCombo->setCurrentIndex(2);
        tableCombo->setCurrentIndex(0);

        // Restrict input to integers
        QIntValidator *intValidator = new QIntValidator();
        timeALine->setValidator(intValidator);
        timeLLine->setValidator(intValidator);
        distanceLine->setValidator(intValidator);

    }

    // Print Article
    ui->article->setText(narticle.toString());


    /*
     * Dynamically add obstacles. Each element has the following widgets:
     * 0 - label
     * 1 - Single radio
     * 2 - Double radio
     * 3 - Treble radio
     * 4 - Double radio
     * 5 - Jo_too checkbox
     * 6 - Jo_too_2 checkbox
    */
    obstaclesList = QList< QList<QWidget*> >();
    connect(ui->obstacles, &QLineEdit::textChanged, this, &SetEvent::updateObstacles);
    // Create 1 obstacle by default
    ui->obstacles->setText("1");
    ui->obstacles->setValidator(new QIntValidator(1, 999));

    // Decide which elements are visible according to the article
    if (narticle == "238.1.1") {
        ui->round2_widget->setVisible(false);
    }

    else if (narticle == "238.1.2") {
        ui->round2_widget->setVisible(false);
    }

    else if (narticle == "238.1.3") {
        ui->round2_widget->setVisible(false);
    }

    else if (narticle == "273.3.1") {
        ui->round2_widget->setVisible(true);
    }


    // Make widgets advance with Enter as well as Tab
    //    connect(ui->Joq, &QLineEdit::returnPressed, this, &SetEvent::focusNextChild);
    connect(ui->obstacles, &QLineEdit::returnPressed, this, &SetEvent::focusNextChild);
    QList<QLineEdit *> allLineEdits = ui->fields_widget->findChildren<QLineEdit *>();
    for (int i = 0; i < allLineEdits.length(); i++) {
        connect(allLineEdits[i], &QLineEdit::returnPressed, this, &SetEvent::focusNextChild);
    }

    connect(ui->lineEdit_4,SIGNAL(textChanged(QString)),this,SLOT(allowButton()));
    connect(ui->lineEdit_5,SIGNAL(textChanged(QString)),this,SLOT(allowButton()));
    connect(ui->lineEdit_6,SIGNAL(textChanged(QString)),this,SLOT(allowButton()));
    connect(ui->r1Distance_line,SIGNAL(textChanged(QString)),this,SLOT(allowButton()));
    connect(ui->r2Distance_line,SIGNAL(textChanged(QString)),this,SLOT(allowButton()));
    connect(ui->obstacles,SIGNAL(textChanged(QString)),this,SLOT(allowButton()));

    QValidator* intValidator1 = new QIntValidator(1,100,this);
    ui->lineEdit_4->setValidator(intValidator1);
}

SetEvent::~SetEvent()
{
    delete ui;
}

void SetEvent::select1stRound()
{
    roundNum=1;
    ui->art245->setVisible(true);
    ui->art274->setVisible(true);
    ui->art274_2->setVisible(true);
    ui->start_combo->setEnabled(false);
    ui->label_4->setEnabled(false);
    ui->checkBox->setEnabled(false);
    ui->checkBox_2->setEnabled(false);
    ui->checkBox_3->setEnabled(false);

    on_checkBox_toggled(true);
}

void SetEvent::select2ndRound()
{
    roundNum=2;
    ui->art245->setVisible(false);
    ui->art274->setVisible(false);
    ui->art274_2->setVisible(false);
    ui->r1_label->setText("2nd Round");
    ui->start_combo->setEnabled(true);
    ui->label_4->setEnabled(true);
    ui->checkBox->setEnabled(true);
    ui->checkBox->setChecked(true);
    ui->checkBox_2->setEnabled(true);
    ui->checkBox_3->setEnabled(true);
    // Add items to the start_combo comboboxes for 2nd round filtered by faults or quantity or %
    ui->start_combo->addItem("According to the penalties");
    ui->start_combo->addItem("According to the penalties and time");
    ui->start_combo->addItem("Reverse order of penalties");
    ui->start_combo->addItem("Reverse order of penalties and time");

    // Add items to the placing_combo comboboxes for 2nd round
    ui->placing_combo->addItem("Penalties in the 2nd Phase (penalties in the 1st Phase)");
    ui->placing_combo->addItem("Penalties and time in the 2nd Phase (penalties in the 1st Phase)");
    ui->placing_combo->addItem("Penalties and time in the 2nd Phase (penalties and time in the 1st Phase)");
    ui->placing_combo->addItem("Total time (Table C) of the 2nd Phase (penalties in the 1stPhase)");
    ui->placing_combo->addItem("Aggregate penalties in both phases (time of the  2nd phase) Art. 274.5.6");

    on_checkBox_toggled(true);
}

void SetEvent::selectJumpOff()
{
    roundNum=3;
    ui->art245->setVisible(false);
    ui->art274->setVisible(false);
    ui->art274_2->setVisible(false);
    ui->r1_label->setText("JUMP-OFF");
    ui->start_combo->setEnabled(true);
    ui->label_4->setEnabled(true);
    ui->checkBox->setEnabled(true);
    ui->checkBox->setChecked(true);
    ui->checkBox_2->setEnabled(true);
    ui->checkBox_3->setEnabled(true);
    // Add items to the start_combo comboboxes for 2nd round filtered by faults or quantity or %
    ui->start_combo->addItem("According to the penalties");
    ui->start_combo->addItem("According to the penalties and time");
    ui->start_combo->addItem("Reverse order of penalties");
    ui->start_combo->addItem("Reverse order of penalties and time");

    // Add items to the placing_combo comboboxes for 2nd round
    ui->placing_combo->addItem("Penalties in the 2nd Phase (penalties in the 1st Phase)");
    ui->placing_combo->addItem("Penalties and time in the 2nd Phase (penalties in the 1st Phase)");
    ui->placing_combo->addItem("Penalties and time in the 2nd Phase (penalties and time in the 1st Phase)");
    ui->placing_combo->addItem("Total time (Table C) of the 2nd Phase (penalties in the 1stPhase)");
    ui->placing_combo->addItem("Aggregate penalties in both phases (time of the  2nd phase) Art. 274.5.6");

    on_checkBox_toggled(true);
}

void SetEvent::allowButton()
{
    bool distance1,distance2;
    distance1 = ui->r1Distance_line->text() !="";
    if(ui->r2Distance_line->isEnabled() && ui->round2_widget->isVisible()){
        distance2 = ui->r2Distance_line->text() !="";
    }else{
        distance2=true;
    }

    bool condition = true;
    if(roundNum>1){
        if(ui->checkBox->isChecked()){
            if(ui->lineEdit_6->text() !=""){
                condition=true;
            }else{
                condition=false;
            }
        }else if(ui->checkBox_2->isChecked()){
            if(ui->lineEdit_5->text() !=""){
                condition=true;
            }else{
                condition=false;
            }
        }else if(ui->checkBox_3->isChecked()){
            if(ui->lineEdit_4->text() !=""){
                condition=true;
            }else{
                condition=false;
            }
        }else{
            condition=false;
        }
    }


    if(condition  && ui->obstacles->text().toInt()>1 && distance1 && distance2){
        ui->pushButton->setEnabled(true);
    }else{
        ui->pushButton->setEnabled(false);
    }
}


void SetEvent::updateWidgetVisibility(QWidget *widget, bool visible) {
    widget->setVisible(visible);
    if (visible) {
        widget->show();
    } else {
        widget->hide();
    }
}


void SetEvent::updateTimes(QComboBox *speed_combo, QLineEdit *distance_line, QLineEdit *timeA_line, QLineEdit *timeL_line){
    QString speed = speed_combo->currentText();
    double distance = distance_line->text().toDouble();
    double x = 5;
    if (speed == "325") {
        x = 5.41;
    }
    else if (speed == "350") {
        x = 5.83;
    }
    else if (speed == "375") {
        x = 6.25;
    }
    else if (speed == "400") {
        x = 6.66;
    }

    float value = distance / x;
    if(ui->r1Table_combo->currentText()=="C"){
        if(distance >=600){
            timeA_line->setText("");
            timeL_line->setText("180");
        }else{
            timeA_line->setText("");
            timeL_line->setText("120");
        }
    }else{
        timeL_line->setText(QString::number(value * 2));
    }
    timeA_line->setText(QString::number(value));

}

void SetEvent::updateObstacles(const QString &text) {
    if (text == "") {
        return;
    }
    int nObstacles = text.toInt();

    if (nObstacles <= 0) {
        return;
    }

    if (nObstacles < obstaclesList.length()) {
        for (int i = obstaclesList.length(); i > nObstacles; i--) {
            auto obstacleWidgets_list = obstaclesList.takeLast();
            //qInfo() << "Deleting " << obstacleWidget->objectName();
            ui->obstacles_scrollLayout->removeWidget(obstacleWidgets_list.last());

            qDeleteAll(obstacleWidgets_list);
            obstacleWidgets_list.clear();
        }
    }

    else {
        for (int i = obstaclesList.length(); i < nObstacles; i++) {
            QWidget* obstacleWidget = new QWidget();
            obstacleWidget->setObjectName("obstacle" + QString::number(i) + "_widget");
            QHBoxLayout* obstacleLayout = new QHBoxLayout();
            QList<QWidget*> obstacleWidgets_list = QList<QWidget*>();

            // 0
            QLabel* obs_label = new QLabel(QString::number(i + 1));
            obs_label->setObjectName("obs" + QString::number(i) + "_label");
            obs_label->setFont(QFont("Sans Serif", 16));
            obstacleWidgets_list.append(obs_label);
            obstacleLayout->addWidget(obs_label);

            // 1
            QRadioButton* obsSingle_radio = new QRadioButton("Single");
            obsSingle_radio->setObjectName("obs" + QString::number(i) + "Single_radio");
            obsSingle_radio->setChecked(true);
            obstacleWidgets_list.append(obsSingle_radio);
            obstacleLayout->addWidget(obsSingle_radio);

            // 2
            QRadioButton* obsDouble_radio = new QRadioButton("Double");
            obsDouble_radio->setObjectName("obs" + QString::number(i) + "Double_radio");
            obstacleWidgets_list.append(obsDouble_radio);
            obstacleLayout->addWidget(obsDouble_radio);

            // 3
            QRadioButton* obsTreble_radio = new QRadioButton("Treble");
            obsTreble_radio->setObjectName("obs" + QString::number(i) + "Treble_radio");
            obstacleWidgets_list.append(obsTreble_radio);
            obstacleLayout->addWidget(obsTreble_radio);

            // 4
            QRadioButton* obsQuad_radio = new QRadioButton("Quad");
            obsQuad_radio->setObjectName("obs" + QString::number(i) + "Quad_radio");
            obstacleWidgets_list.append(obsQuad_radio);
            obstacleLayout->addWidget(obsQuad_radio);


            // 7
            obstacleWidget->setLayout(obstacleLayout);
            obstacleWidgets_list.append(obstacleWidget);
            obstaclesList.append(obstacleWidgets_list);

            ui->obstacles_scrollLayout->addWidget(obstacleWidget);
        }
    }

    // Let the first element of the list of obstacles grab the focus of the scroll area.
    ui->obstacles_scroll->setFocusProxy(obstaclesList[0][0]);
    //    decideFromJoq(ui->Joq->text());
}

void SetEvent::on_pushButton_clicked()
{
    Setting *setting = new Setting();
    setting->strArticle = ui->article->text();
    if (setting->strArticle == "238.1.1") {
        setting->p1stRound = new RoundSetting();
        setting->p1stRound->distance = ui->r1Distance_line->text().toDouble();
        setting->p1stRound->timeallowed = ui->r1TimeA_line->text().toDouble();
        setting->p1stRound->timelimit = ui->r1TimeL_line->text().toDouble();
        setting->p1stRound->cTableCompetition = ui->r1Table_combo->currentText().at(0).toLatin1();
    }

    else if (setting->strArticle == "238.1.2") {
        setting->p1stRound = new RoundSetting();
        setting->p1stRound->distance = ui->r1Distance_line->text().toDouble();
        setting->p1stRound->timeallowed = ui->r1TimeA_line->text().toDouble();
        setting->p1stRound->timelimit = ui->r1TimeL_line->text().toDouble();
        setting->p1stRound->cTableCompetition = ui->r1Table_combo->currentText().at(0).toLatin1();
    }

    else if (setting->strArticle == "238.1.3") {
        setting->p1stRound = new RoundSetting();
        setting->p1stRound->distance = ui->r1Distance_line->text().toDouble();
        setting->p1stRound->timeallowed = ui->r1TimeA_line->text().toDouble();
        setting->p1stRound->timelimit = ui->r1TimeL_line->text().toDouble();
        setting->p1stRound->cTableCompetition = ui->r1Table_combo->currentText().at(0).toLatin1();
    }

    else if (setting->strArticle == "273.3.1") {
        setting->p1stRound = new RoundSetting();
        setting->p1stRound->distance = ui->r1Distance_line->text().toDouble();
        setting->p1stRound->timeallowed = ui->r1TimeA_line->text().toDouble();
        setting->p1stRound->timelimit = ui->r1TimeL_line->text().toDouble();
        setting->p1stRound->cTableCompetition = ui->r1Table_combo->currentText().at(0).toLatin1();
        setting->p2ndRound = new RoundSetting();
        setting->p2ndRound->distance = ui->r2Distance_line->text().toDouble();
        setting->p2ndRound->timeallowed = ui->r2TimeA_line->text().toDouble();
        setting->p2ndRound->timelimit = ui->r2TimeL_line->text().toDouble();
        setting->p2ndRound->cTableCompetition = ui->r2Table_combo->currentText().at(0).toLatin1();
    }

    for (int i = 0; i < obstaclesList.length(); i++) {
        QList<int> obsData;
        for (int j = 1; j <= 4; j++) {
            QRadioButton *radio = (QRadioButton*) obstaclesList[i][j];
            if (radio->isChecked()) {
                obsData.append(j);
                break;
            }
        }

        setting->obstacleData.append(obsData);
    }

    /*
     * data:
     * 0 - Obstacle type: 1 (single), 2 (double), 3 (treble) or 4 (quadre).
     * 1 - Whether first jump-off (Jo_too) is selected: 0 (false) or 1 (true).
     * 2 - Whether second jump-off (Jo_too_2) is selected: 0 (false) or 1 (true).
     */
    setting->startComboText = ui->start_combo->currentText();
    setting->faultCheck = ui->checkBox->isChecked();
    setting->riderCheck = ui->checkBox_2->isChecked();
    setting->percentCheck = ui->checkBox_3->isChecked();
    setting->faultNum = ui->lineEdit_6->text().toInt();
    setting->riderNum = ui->lineEdit_5->text().toInt();
    setting->percentNum = ui->lineEdit_4->text().toInt();
    setting->placingComboText = ui->placing_combo->currentText();

    emit saved(setting);
    close();
}

void SetEvent::on_art245_clicked()
{
    if(ui->art245->isChecked()){
        ui->round2_widget->setVisible(true);
        ui->round2_widget->setEnabled(true);
        ui->r1_label->setText("1st Round");
        ui->r2_label->setText("Jump-Off");
        ui->art274->setChecked(false);
        ui->art274->setEnabled(false);
        ui->art274_2->setChecked(false);
        ui->art274_2->setEnabled(false);
    }else{
        ui->art274->setEnabled(true);
        ui->art274_2->setEnabled(true);
        ui->round2_widget->setVisible(false);
        ui->round2_widget->setEnabled(false);
    }
}

void SetEvent::on_art274_clicked()
{
    if(ui->art274->isChecked()){
        ui->round2_widget->setVisible(true);
        ui->round2_widget->setEnabled(true);
        ui->r1_label->setText("1st Phase");
        ui->r2_label->setText("2nd Phase");
        ui->art245->setChecked(false);
        ui->art245->setEnabled(false);
        ui->art274_2->setChecked(false);
        ui->art274_2->setEnabled(false);
    }else{
        ui->art245->setEnabled(true);
        ui->art274_2->setEnabled(true);
        ui->round2_widget->setVisible(false);
        ui->round2_widget->setEnabled(false);
    }
}

void SetEvent::on_art274_2_clicked()
{
    if(ui->art274_2->isChecked()){
        ui->round2_widget->setVisible(true);
        ui->round2_widget->setEnabled(true);
        ui->r1_label->setText("1st Phase");
        ui->r2_label->setText("2nd Phase");
        ui->art245->setChecked(false);
        ui->art245->setEnabled(false);
        ui->art274->setChecked(false);
        ui->art274->setEnabled(false);
    }else{
        ui->art245->setEnabled(true);
        ui->art274->setEnabled(true);
        ui->round2_widget->setVisible(false);
        ui->round2_widget->setEnabled(false);
    }
}

void SetEvent::on_clock_toggled(bool checked)
{
    if(checked){
        ui->clock->setIcon(QIcon(":/imgs/res/checkbox_yes.png"));
    }else{
        ui->clock->setIcon(QIcon(":/imgs/res/checkbox_no.png"));
    }
    ui->clock->setIconSize(QSize(30,30));
}

void SetEvent::on_clock_2_toggled(bool checked)
{
    if(checked){
        ui->clock_2->setIcon(QIcon(":/imgs/res/checkbox_yes.png"));
    }else{
        ui->clock_2->setIcon(QIcon(":/imgs/res/checkbox_no.png"));
    }
}

void SetEvent::on_checkBox_toggled(bool checked)
{
    if(checked){
        ui->lineEdit_6->setEnabled(true);
        ui->lineEdit_6->setText("0");
        ui->label_9->setEnabled(true);
        ui->lineEdit_5->setEnabled(false);
        ui->label_10->setEnabled(false);
        ui->lineEdit_4->setEnabled(false);
        ui->label_11->setEnabled(false);
        ui->checkBox_2->setEnabled(false);
        ui->checkBox_2->setChecked(false);
        ui->checkBox_3->setEnabled(false);
        ui->checkBox_3->setChecked(false);
    }else{
        ui->lineEdit_6->setEnabled(false);
        ui->lineEdit_6->clear();
        ui->label_9->setEnabled(false);
        ui->checkBox_2->setEnabled(true);
        ui->checkBox_3->setEnabled(true);
    }
    allowButton();
}

void SetEvent::on_checkBox_2_toggled(bool checked)
{
    if(checked){
        ui->lineEdit_6->setEnabled(false);
        ui->label_9->setEnabled(false);
        ui->lineEdit_5->setEnabled(true);
        ui->label_10->setEnabled(true);
        ui->lineEdit_4->setEnabled(false);
        ui->label_11->setEnabled(false);
        ui->checkBox->setEnabled(false);
        ui->checkBox->setChecked(false);
        ui->checkBox_3->setEnabled(false);
        ui->checkBox_3->setChecked(false);
    }else{
        ui->lineEdit_5->setEnabled(false);
        ui->label_10->setEnabled(false);
        ui->checkBox->setEnabled(true);
        ui->checkBox_3->setEnabled(true);
    }
    allowButton();
}

void SetEvent::on_checkBox_3_toggled(bool checked)
{
    if(checked){
        ui->lineEdit_6->setEnabled(false);
        ui->label_9->setEnabled(false);
        ui->lineEdit_5->setEnabled(false);
        ui->label_10->setEnabled(false);
        ui->lineEdit_4->setEnabled(true);
        ui->label_11->setEnabled(true);
        ui->checkBox->setEnabled(false);
        ui->checkBox->setChecked(false);
        ui->checkBox_2->setEnabled(false);
        ui->checkBox_2->setChecked(false);
    }else{
        ui->lineEdit_4->setEnabled(false);
        ui->label_11->setEnabled(false);
        ui->checkBox->setEnabled(true);
        ui->checkBox->setEnabled(true);
        ui->checkBox_2->setEnabled(true);
    }
    allowButton();
}

void SetEvent::on_r1Table_combo_activated(const QString &arg1)
{
    if(arg1 == "A"){
        ui->label_5->setText("Every");
        ui->label_6->setText("Sec.");
        ui->r1TimeA_label->setEnabled(true);
        ui->r1TimeA_line->setEnabled(true);
        emit ui->r1Distance_line->textChanged(ui->r1Distance_line->text());
    }else if(arg1 == "C"){
        ui->label_5->setText("Add");
        ui->label_6->setText("Sec.");
        ui->r1TimeA_label->setEnabled(false);
        ui->r1TimeA_line->setEnabled(false);
        emit ui->r1Distance_line->textChanged(ui->r1Distance_line->text());
    }
}

void SetEvent::on_r2Table_combo_activated(const QString &arg1)
{
    if(arg1 == "A"){
        ui->label_7->setText("Every");
        ui->label_8->setText("Sec.");
        ui->r2TimeA_label->setEnabled(true);
        ui->r2TimeA_line->setEnabled(true);
        emit ui->r2Distance_line->textChanged(ui->r2Distance_line->text());
    }else if(arg1 == "C"){
        ui->label_7->setText("Add");
        ui->label_8->setText("Sec.");
        ui->r2TimeA_label->setEnabled(false);
        ui->r2TimeA_line->setEnabled(false);
        emit ui->r2Distance_line->textChanged(ui->r2Distance_line->text());
    }
}
