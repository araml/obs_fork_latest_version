#include "tabledelegate.h"
#include <QMimeData>
#include "tablemodelparticipants.h"
#include <QDebug>
TableDelegate::TableDelegate(QObject *parent) {
}

void TableDelegate::paint(QPainter *painter,
                          const QStyleOptionViewItem &option,
                          const QModelIndex &index) const {
//    if (index.column() == 4 || index.column() == 9) {
//        QPixmap pixmap = QPixmap();
//        if (pixmap.loadFromData(index.data().toByteArray())) {
//            QPixmap scaledPixmap = pixmap.scaled(option.rect.size(), Qt::KeepAspectRatio);
//            int pixmapX = option.rect.center().x() - int(scaledPixmap.width() / 2);
//            painter->drawPixmap(pixmapX, option.rect.y(), scaledPixmap);
//        } else {
//            QStyledItemDelegate::paint(painter, option, index);
//        }
//    }
//    else if (index.row() == 0) {
//        QFont font = painter->font();
//        font.setPointSize(20);
//        painter->setFont(font);
//        painter->setPen(QColor(Qt::yellow));
//        painter->drawText(option.rect, index.data().toString());
//    }
//    else {
//        QStyledItemDelegate::paint(painter, option, index);
//    }

    QString strTmp = "";
    TableModelParticipants *pModel = (TableModelParticipants*)index.model();
    if(pModel == NULL)
        return;

    QStyledItemDelegate::paint(painter,option,index);

    painter->save();

    QFont font = painter->font();
    QFont SubFont = painter->font();

    if(index.row() == 0)
    {
        font.setBold(true);
        QBrush brush(QColor(120,150,80,255));
        painter->fillRect(option.rect,brush);
        if(option.state & QStyle::State_Selected){
            painter->setPen(QColor(Qt::red));
            painter->drawRect(option.rect);
        }
        painter->setPen(QColor(Qt::yellow));
    }
    else
    {
        font.setBold(false);
        painter->setPen(QColor(Qt::white));
    }
    SubFont.setWeight(SubFont.weight()-2);
    QFontMetrics fm(font);

    QSize iconsize;
    iconsize.setWidth(60);
    iconsize.setHeight(60);

    QRect headerRect = option.rect;
    QRect subheaderRect = option.rect;
    QRect riderImgRect = subheaderRect;
    QRect houseImgRect = subheaderRect;
    QRect posRect = subheaderRect;

    posRect.setRight(posRect.right() - 25);
    posRect.setLeft(posRect.right() - 40);
    posRect.setTop(posRect.center().y() - 20);
    posRect.setBottom(posRect.top() + 40);

    painter->drawEllipse(posRect);
    strTmp = pModel->index(index.row(),pModel->fieldIndex("in_pos")).data().toString();
    painter->drawText(posRect,Qt::AlignHCenter|Qt::AlignVCenter,strTmp);

    riderImgRect.setRight(iconsize.width());
    riderImgRect.setTop(riderImgRect.top()+5);
    riderImgRect.setBottom(riderImgRect.top() + iconsize.height());

    houseImgRect = riderImgRect;
    houseImgRect.moveLeft(64);

    headerRect.setLeft(houseImgRect.right() + 4);
    subheaderRect.setLeft(houseImgRect.right() + 4);
    headerRect.setTop(headerRect.top()+5);
    headerRect.setBottom(headerRect.top()+fm.height());

    subheaderRect.setTop(headerRect.bottom()+2);

    QPixmap pixmap = QPixmap();
    //Rider Image
    if (pixmap.loadFromData(pModel->index(index.row(),pModel->fieldIndex("rider_img")).data().toByteArray())) {
        QPixmap scaledPixmap = pixmap.scaled(QSize(60,60), Qt::KeepAspectRatio);

        painter->drawPixmap(0, option.rect.y() + 5, scaledPixmap);
    }
    else {
        QIcon icon(":/imgs/rider_avatar");
        painter->drawPixmap(riderImgRect,icon.pixmap(iconsize.width(),iconsize.height()));
    }
    //Horse Image
    if (pixmap.loadFromData(pModel->index(index.row(),pModel->fieldIndex("horse_img")).data().toByteArray())) {
        QPixmap scaledPixmap = pixmap.scaled(QSize(60,60), Qt::KeepAspectRatio);

        painter->drawPixmap(houseImgRect.left(), option.rect.y() + 5, scaledPixmap);
    }
    else {
        QIcon icon(":/imgs/horse_avatar");
        painter->drawPixmap(houseImgRect,icon.pixmap(iconsize.width(),iconsize.height()));
    }

    //Rider name
    strTmp = pModel->index(index.row(),pModel->fieldIndex("Name")).data().toString();
    painter->setFont(font);
    painter->drawText(headerRect,strTmp);

    //Cat
    strTmp = pModel->index(index.row(),pModel->fieldIndex("cat")).data().toString();
    painter->setFont(SubFont);
    painter->setPen(QColor(Qt::white));
    painter->drawText(subheaderRect.left(),subheaderRect.top()+17,"Cat: " + strTmp);

    strTmp = pModel->index(index.row(),pModel->fieldIndex("name_H")).data().toString();
    painter->drawText(subheaderRect.left(),subheaderRect.top()+34,"Horse: " + strTmp);

    painter->restore();
}

QSize TableDelegate::sizeHint(const QStyleOptionViewItem &option,
                              const QModelIndex &index) const {
//    if (index.column() == 4 || index.column() == 9) {
//        QPixmap pixmap = QPixmap();
//        if (pixmap.loadFromData(index.data().toByteArray())) {
//            return pixmap.scaled(option.rect.size(), Qt::KeepAspectRatio).size();
//        } else {
//            return QStyledItemDelegate::sizeHint(option, index);
//        }
//    }
//    return QStyledItemDelegate::sizeHint(option, index);
    Q_UNUSED(index)
    const int width = option.rect.width();
    const int height = 70;
    return QSize(width, height);
}
