#include "tablemodelparticipants.h"
#include <QIODevice>
#include <QDataStream>
#include <QDebug>
#include <QSqlRecord>

TableModelParticipants::TableModelParticipants()
    :QSqlTableModel()
{
}
TableModelParticipants::TableModelParticipants(QObject *parent, QSqlDatabase db)
    :QSqlTableModel(parent,db)
{

}

Qt::ItemFlags TableModelParticipants::flags(const QModelIndex &index) const
{
    Qt::ItemFlags defaultFlags = QSqlTableModel::flags(index);

    if (index.isValid())
        return Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled | defaultFlags;
    else
        return Qt::ItemIsDropEnabled | defaultFlags;
}
Qt::DropActions TableModelParticipants::supportedDropActions() const
{
    return Qt::MoveAction;
}

QMimeData *TableModelParticipants::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = QSqlTableModel::mimeData(indexes);
    mimeData->setData("application/custom.text.rowid",QString::number(indexes.at(0).row()).toLocal8Bit());
    return mimeData;
}
bool TableModelParticipants::dropMimeData(const QMimeData *data,
                                   Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    Q_UNUSED(column);

    if (action == Qt::IgnoreAction)
        return true;

    int beginRow;
    qDebug()<< "row: " << row;
    if (row != -1)
    {
        beginRow = row;
    }
    else if (parent.isValid())
    {
        beginRow = parent.row();
    }
    else
    {
        beginRow = rowCount(QModelIndex());
    }

    int srcRow = data->data("application/custom.text.rowid").toInt();
    qDebug() <<"SRC row:"<<srcRow;
    qDebug() <<"Dsc row:"<<beginRow;

    qDebug() << "row count: " << rowCount();
    QModelIndex idx1 = this->index(srcRow, fieldIndex("no_order"));
    this->setData(idx1,beginRow+1,Qt::EditRole);
    int newid = 1;
    for(int d = 0; d < rowCount(); d++){
        QModelIndex idx = this->index(d, fieldIndex("no_order"));
        if((beginRow > 0 && d == beginRow+1) || (d == 0 && d == beginRow)) newid++;
        if(d == srcRow){
            continue;
        }
        this->setData(idx,newid++,Qt::EditRole);
    }
    submitAll();
    select();
    return true;
}
