#include "confirmendroutedlg.h"
#include "ui_confirmendroutedlg.h"
#include <QMessageBox>
#include <QDoubleValidator>
#include <QMediaPlayer>
//#include <QSlider>
//#include <QHBoxLayout>

ConfirmEndRouteDlg::ConfirmEndRouteDlg(QWidget *parent) :
    QDialog(parent,Qt::CustomizeWindowHint ),
    ui(new Ui::ConfirmEndRouteDlg)
{
    ui->setupUi(this);

    ui->ledFaults->setMaxLength(8);
    ui->ledTime->setMaxLength(8);
    ui->ledFaults->setValidator(new QDoubleValidator(0, 4, 3, this));
    ui->ledTime->setValidator(new QDoubleValidator(0, 4, 3, this));

    connect(ui->pbtnCancel,SIGNAL(clicked(bool)),this,SLOT(onCancel()));
    connect(ui->pbtnOk,SIGNAL(clicked(bool)),this,SLOT(onOK()));
}

void ConfirmEndRouteDlg::setValues(QString faults, QString time, QString penalties, QString total)
{
    ui->ledFaults->setText(faults);
    ui->ledTime->setText(time);
    ui->ledpenalty->setText(penalties);
    ui->ledTot_faults->setText(total);
}

ConfirmEndRouteDlg::~ConfirmEndRouteDlg()
{
    delete ui;
}

double ConfirmEndRouteDlg::getFaultsValue()
{
    return ui->ledFaults->text().toDouble();
}

double ConfirmEndRouteDlg::getTimeValue()
{
    return ui->ledTime->text().toDouble();
}
double ConfirmEndRouteDlg::getTotalFaultsValue(){
    return ui->ledTot_faults->text().toDouble();
}
double ConfirmEndRouteDlg::getPenaltyValue(){
    return ui->ledpenalty->text().toDouble();
}
void ConfirmEndRouteDlg::onOK()
{
    QDialog::accept();
}

void ConfirmEndRouteDlg::onCancel()
{
    QMessageBox::StandardButton result = QMessageBox::question(this, "Confirmation", QString("Are you sure?"),
                                                                                   QMessageBox::Yes|
                                                                                   QMessageBox::No);
    if(result == QMessageBox::Yes)
    {
        QDialog::reject();
    }
}
void ConfirmEndRouteDlg::closeEvent (QCloseEvent *event)
{
    onCancel();
}

void ConfirmEndRouteDlg::on_review_clicked()
{
//    m_positionSlider = new QSlider(Qt::Horizontal);
//    m_positionSlider->setRange(0, 0);

    QMediaPlayer* playerv = new QMediaPlayer;
    QVideoWidget *vw = new QVideoWidget;

/*    QSlider *slider = new QSlider(Qt::Horizontal);

    QHBoxLayout *layout = new QHBoxLayout;
         layout->addWidget(slider);
         layout->addWidget(vw);
*/
    playerv->setVideoOutput(vw);
    playerv->setMedia(QUrl::fromLocalFile("C:/Qt/Qt5.10.1/Nuevo/build_principal_17/riderout.mp4"));
//    QMediaPlayer addWidget(slider);
    playerv->setMuted(true);
//    slider->setGeometry(10,16,820,630);
    //    connect(playerv, SIGNAL(positionChanged(qint64)), SLOT(positionChanged(qint64)));
//    playerv->setPosition();
//    playerv->QSlider(m_positionSlider);
    vw->setGeometry(500,150,800,600);
    vw->show();
    playerv->play();

}
