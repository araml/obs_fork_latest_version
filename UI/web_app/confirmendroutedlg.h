#ifndef CONFIRMENDROUTEDLG_H
#define CONFIRMENDROUTEDLG_H

#include <QDialog>
#include <QMediaPlayer>
#include <QVideoWidget>


namespace Ui {
class ConfirmEndRouteDlg;
}

class ConfirmEndRouteDlg : public QDialog
{
    Q_OBJECT

public:
    explicit ConfirmEndRouteDlg(QWidget *parent = 0);
    ~ConfirmEndRouteDlg();

    void setValues(QString faults, QString time, QString penalties, QString total);
    double getTimeValue();
    double getFaultsValue();
    double getTotalFaultsValue();
    double getPenaltyValue();
protected:
    void closeEvent (QCloseEvent *event);
private slots:
    void onOK();
    void onCancel();
//    void positionChanged(qint64 position);

    void on_review_clicked();

private:
    Ui::ConfirmEndRouteDlg *ui;
//        QMediaPlayer* m_mediaPlayer;
        QVideoWidget* vw;
        //        QSlider *m_positionSlider;

};

#endif // CONFIRMENDROUTEDLG_H
