#include "roundselectiondialog.h"
#include "ui_roundselectiondialog.h"

RoundSelectionDialog::RoundSelectionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RoundSelectionDialog)
{
    ui->setupUi(this);
}

RoundSelectionDialog::~RoundSelectionDialog()
{
    delete ui;
}

void RoundSelectionDialog::on_pushButton_clicked()
{
    emit changeRound(1);
    close();
}

void RoundSelectionDialog::on_pushButton_2_clicked()
{
    emit changeRound(2);
    close();
}

void RoundSelectionDialog::on_pushButton_3_clicked()
{
    emit changeRound(3);
    close();
}
