#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "confirmendroutedlg.h"
#include <iostream>
#include <QSound>
#include "roundselectiondialog.h"
#include <QSettings>
#include <QDateTime>
#include <QUrl>
#include <QSsl>
#include <string>
#include <QTextStream>
// PDE
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <iostream>
#include <algorithm>
#include <QTextStream>
#include <QWebEngineSettings>
//database manager
#include "database_manager.h"
#include <QMutexLocker>
#include <QThread>
#include <stdexcept>

#include <obs-app.hpp>

#include <error_log.h>
using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_QuitOnClose);

    // NEW SERIAL from PDE_COVER
    const auto serialPortInfos = QSerialPortInfo::availablePorts();
    QString description;
    QString manufacturer;
    QString serialNumber;
    QString tty;

    log_pde("Total number of ports available: %i\n", serialPortInfos.count());

#if 0
    #ifdef _WIN32
        QString company_name =  "Prolific";
    #elif linux
        QString company_name = "Prolific Technology Inc.";
    #endif
#else
    // Serial que se comunica por RF.
    #ifdef _WIN32
        QString company_name =  "Silicon Labs";
    #elif linux
        QString company_name = "Silicon Labs";
    #endif
#endif

    for (const QSerialPortInfo &serialPortInfo : serialPortInfos) {
        description = serialPortInfo.description();
        manufacturer = serialPortInfo.manufacturer();
        serialNumber = serialPortInfo.serialNumber();

        log_pde("Description %s\n", description.toStdString().c_str());
        log_pde("Manufacturer %s\n", manufacturer.toStdString().c_str());
        log_pde("serialNumber %s\n", serialNumber.toStdString().c_str());

        if (manufacturer == company_name) {
#ifdef _WIN32
            tty = serialPortInfo.portName();
#elif linux
            tty = "/dev/" + serialPortInfo.portName();
#endif
            break;
        }
    }

    port.setPortName(tty); // tty
    port.setBaudRate(QSerialPort::Baud9600);
    log_pde("Selected port \"%s\" for connection.\n", tty.toStdString().c_str());

    connect(&port, &QSerialPort::readyRead, this, &MainWindow::handle_read);

    if (!port.open(QIODevice::ReadOnly)) {
        log_pde("Error when opening port \"%s\".\n", tty.toStdString().c_str());
    } else {
        log_pde("Port \"%s\" opened successfully.\n",
                tty.toStdString().c_str());
    }
    // END NEW SERIAL from PDE_COVER

    QSettings settings("settings.ini",QSettings::IniFormat);

    // load saved variables from file
    c_country = settings.value("c_country").toString();
    c_event_place = settings.value("c_event_place").toString();
    c_place_code = settings.value("c_place_code").toString();
    c_height = "";

    state = INITIAL; //nuevo

    mFaultAsTime = 0;
    mElapseTime = 0;
    mTimeAllow = 0;
    mTimeLimit = 0;
    mAgainstClock = 0;
    mTableCompl = 'A';
    bBelRingging = false;
    bs = 0;
    firsttime = true;

/*    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setUserName("root");
    db.setPassword("");
    db.setDatabaseName("flacoesp_eque");
    db.setPort(3306);
    db.setConnectOptions("MYSQL_OPT_RECONNECT=1;MYSQL_OPT_CONNECT_TIMEOUT=86400;MYSQL_OPT_READ_TIMEOUT=60");
    db.open();
*/
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("www.jumpemirates.com");
    db.setUserName("flacoespil");
    db.setPassword("flaquito");
    db.setDatabaseName("flacoesp_eque");
    db.setPort(3306);
    db.setConnectOptions("MYSQL_OPT_RECONNECT=1;MYSQL_OPT_CONNECT_TIMEOUT=30;MYSQL_OPT_READ_TIMEOUT=60");
    db.open();

    if(!db.open()){
        qDebug() << "DB NOT OPEN";
        qDebug() << db.lastError().text();
    }
//upload
//        m_manager = new QNetworkAccessManager(this);
//end upload
//    QSqlQuery query(DatabaseManager::database());

    argEventQuery = new QSqlQuery;
    argEventQuery->exec("SELECT event_N, event_name, height, def, article, event_id FROM "+c_country.toLower()+"event WHERE country = '"+c_country+"' AND event_place = '"+c_event_place+"' AND date_event >= CURDATE()");
    //    c_height = height;

    QString combined, event_name, height, def, article;
    while(argEventQuery->next()) {
        // Global variables
        event_id = argEventQuery->value("event_id").toString().toLower();
        event_N = argEventQuery->value("event_N").toString();
/*        if (event_N.length() == 1) {
            event_N = "0" + event_N;
        }
*/        // Local vars
        event_name = argEventQuery->value("event_name").toString();
        height = argEventQuery->value("height").toString();
        def = argEventQuery->value("def").toString();
        article = argEventQuery->value("article").toString();
        QString s1,s2,s3,s4;
        int i =0;
        while(i<(4-event_N.count())*2){
            i++;
            s1+=" ";
        }
        int j=0;
        while(j<(30-event_name.count())*2){
            j++;
            s2+=" ";
        }
        int k =0;
        while(k<(4-height.count())*2){
            k++;
            s3+=" ";
        }
        int m=0;
        while(m<(5-def.count())*2){
            m++;
            s4+=" ";
        }
        combined = event_N + s1 + event_name + s2 + height + s3 + def + s4 + article;

        ui->p_select->addItem(combined, QVariant(article));
    }

    obstaclesButtons = QList <QWidget*>();
    timer = new QTimer();
    timer->setTimerType(Qt::PreciseTimer);
    timer->setInterval(3);
    connect(timer,SIGNAL(timeout()),this,SLOT(update_countdown()));

    // Set buttons spacing
    ui->faults_widgetLayout->setSpacing(5);

    // Hide buttons when theres no event selected
    ui->go_button->hide();
    ui->start_button->hide();
    ui->pause->setEnabled(false);
    ui->pause->setText("Pause");

    mModel = new TableModelParticipants(this);
    //    resultsModel = new QSqlTableModel(this);
    set_table(NULL);
    tableDelegate = new TableDelegate(this);
    ui->lstRiders->setItemDelegate(tableDelegate);
    ui->lstRiders->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->lstRiders->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->lstRiders->setDragDropMode(QAbstractItemView::InternalMove);
    ui->lstRiders->setDragEnabled(true);
    ui->lstRiders->setAcceptDrops(true);
    ui->lstRiders->setDropIndicatorShown(true);
    ui->lstRiders->setDefaultDropAction(Qt::MoveAction);
    //ui->lstRiders->setDragDropOverwriteMode(true);

    // When the selection in the combo box changes, open the set event window
    connect(ui->p_select,SIGNAL(activated(int)),this,SLOT(go_set(int)));
    connect(&mMediaPlayer,SIGNAL(mediaStatusChanged(QMediaPlayer::MediaStatus)),this,SLOT(on_mediaPlayer_mediaStatusChanged(QMediaPlayer::MediaStatus)));
    update_results();
}

QMutex DatabaseManager::s_databaseMutex;
QHash<QThread*, QHash<QString, QSqlDatabase>> DatabaseManager::s_instances;

QSqlDatabase DatabaseManager::database(const QString& connectionName)
{
    QMutexLocker locker(&s_databaseMutex);
    QThread *thread = QThread::currentThread();

    // if we have a connection for this thread, return it
    QHash<QThread*, QHash<QString, QSqlDatabase> >::Iterator it_thread = s_instances.find(thread);
 //   auto it_thread = s_instances.find(thread);
    if (it_thread != s_instances.end()) {
        auto it_conn = it_thread.value().find(connectionName);
        if (it_conn != it_thread.value().end()) {
            return it_conn.value();
        }
    }

    // otherwise, create a new connection for this thread
    QSqlDatabase connection = QSqlDatabase::cloneDatabase(
        QSqlDatabase::database(connectionName),
        QString("%1_%2").arg(connectionName).arg((int64_t)thread));

    // open the database connection
    // initialize the database connection
    if (!connection.open()) {
        qDebug() << "Unable to open the new database connection.";
    }else {
        qDebug() << "table reconnected";
}
    qDebug() << "Function Name: " << Q_FUNC_INFO << " new SQL connection instances Thread: " << thread->currentThreadId() << " Name: " << connectionName;

    s_instances[thread][connectionName] = connection;
    return connection;
}


MainWindow::~MainWindow()
{
    delete ui;
#if QT_VERSION >= 0x050500
    qInfo("Deleting MainWindow");
#endif

   //QApplication::quit();
}


// PDE SERIAL
void MainWindow::handle_read() {
//    if ((bs == 1)){ //bs means barrier bs=0 barrier ignored, bs=1 start barrier ready, bs=2 stop ready
    string data = port.readAll().toStdString();

    if (start_mode) {
        for (size_t i = 0; i < data.length() && idx < start.length(); i++, idx++) {
            if (data[i] != start[idx]) {
                idx = 0;
                return;
            }
            if (idx == start.length() - 1) {
                start_mode = !start_mode;
                idx = 0;
                if ((bs == 1)){  //bs means barrier start
                on_start_button_clicked();
                // here star timer -15% time allowed
                // habilitar barrera 2
                            bs = 2;   // enable stop barrier
                }
            }
        }
    } else {
        for (size_t i = 0; i < data.length() && idx < stop.length(); i++, idx++) {
            if (data[i] != stop[idx]) {
                idx = 0;
                return;
            }
            if (idx == stop.length() - 1) {
                start_mode = !start_mode;
                idx = 0;
                if ((bs==2)) {
                    bs = 0;  //reset barriers
                    on_end_button_clicked();
                }
            }
        }
    }
    //   }
}

// PDE:
void MainWindow::closeEvent(QCloseEvent *event) {
  emit close_app();
}

//END SERIAL

void MainWindow::reset_counters() {
    ui->go_button->setVisible(true);
    ui->go_button->show();
    ui->go_button->setEnabled(true);

    ui->start_button->setVisible(false);
    ui->start_button->hide();

    ui->pause->setEnabled(false);
    ui->pause->setText("Pause");


    ui->elapsed_time->display(-45);
    ui->elapsed_time->setDigitCount(5);

    ui->faults->display(0);
    ui->faults->setDigitCount(3);
    ui->tot_faults->display(0);
    ui->tot_time->display(0);
    ui->penalty->display(0);

    mElapseTime = -45.0;
    mFaultAsTime = 0;
    bBelRingging = false;

    state = INITIAL;
}
void MainWindow::alertbarrier(){
    if(mElapseTime >= (mTimeAllow * 0.80)){
        //        QSound::play(":/sounds/res/bell.wav");
    }
}


void MainWindow::set_table(Setting *setting = NULL) {
//    QSqlQuery query1(DatabaseManager::database());
//    QSqlDatabase dbc = DatabaseManager::database("MYSQL");
//    QSqlQuery query(dbc);
    QSqlQuery query1, query2,query3;
    QString replaced = QString(c_country+c_place_code+"event").toLower();
    query1.exec("UPDATE "+event_id + event_N+" SET no_order = NULL");
    // Select elements that go in the table view
    mModel->setTable(event_id + event_N);
    mModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    QStringList filterList;
    QString orderBy;
    if(roundNum==1){
        filterList<<"fault IS NULL AND time IS NULL";
//        orderBy+="ORDER BY no_order asc"; //cambie para ver el orden
        orderBy+="ORDER BY no_order asc, in_pos asc"; // inverted in_pos to last order seams work
    }else if(roundNum==2){
        filterList<<"2fault IS NULL AND 2time IS NULL";
    }
    bool percent=false;
    if(setting !=NULL){
        if(setting->startComboText!=""){
            if(setting->startComboText=="According to the penalties"){
                orderBy="ORDER BY tot_faults asc, no_order asc";
                if(setting->faultCheck){
                    filterList<<"time>0 AND tot_faults <="+QString::number(setting->faultNum);
                }else if(setting->riderCheck){
                    filterList<<"time>0";
                    orderBy+=" LIMIT "+QString::number(setting->riderNum);
                }else if(setting->percentCheck){
                    percent=true;
                }
            }else if(setting->startComboText=="According to the penalties and time"){
                orderBy="ORDER BY tot_faults ASC, time ASC, no_order asc";
                if(setting->faultCheck){
                    filterList<<"time>0 AND tot_faults <="+QString::number(setting->faultNum);
                }else if(setting->riderCheck){
                    filterList<<"time>0";
                    orderBy+=" LIMIT "+QString::number(setting->riderNum);
                }else if(setting->percentCheck){
                    percent=true;
                }
            }else if(setting->startComboText=="Reverse order of penalties"){
                orderBy="ORDER BY tot_faults desc, no_order asc";
                if(setting->faultCheck){
                    filterList<<"time>0 AND tot_faults <="+QString::number(setting->faultNum);
                }else if(setting->riderCheck){
                    filterList<<"time>0";
                    orderBy+=" LIMIT "+QString::number(setting->riderNum);
                }else if(setting->percentCheck){
                    percent=true;
                }
            }else if(setting->startComboText=="Reverse order of penalties and time"){
                orderBy="ORDER BY tot_faults DESC, time DESC, no_order asc";
                if(setting->faultCheck){
                    filterList<<"time>0 AND tot_faults <="+QString::number(setting->faultNum);
                }else if(setting->riderCheck){
                    filterList<<"time>0";
                    orderBy+=" LIMIT "+QString::number(setting->riderNum);
                }else if(setting->percentCheck){
                    percent=true;
                }
            }
        }
    }

    if(roundNum==2){
        filterList<<"tot_time<7776";
    }

    QString filterString = filterList.join(" AND ");
//    qDebug()<<filterString;

    mModel->setFilter(filterString+" "+orderBy);
    mModel->select();
//    qDebug()<<mModel->lastError();
    qDebug()<<mModel->query().lastError();
    qDebug()<<mModel->query().lastQuery();
    if(percent){
        int total_riders=mModel->rowCount();
        qDebug()<<"total: "<<total_riders;
        int riders = qFloor(qreal(total_riders * setting->percentNum / 100));
        qDebug()<<"riders: "<<riders;
        mModel->setFilter(filterString+" "+orderBy+" LIMIT "+QString::number(riders));
        mModel->select();
//        qDebug()<<mModel->lastError();
//        qDebug()<<mModel->query().lastError();
        qDebug()<<mModel->query().lastQuery();
    }

//    QSqlQuery query;
    query2.exec ("UPDATE "+replaced+" SET test = '" + event_id + event_N + "' WHERE Id=1");
    if(setting != NULL){
        if(roundNum==1){
        if(setting->placingComboText == "According to the penalties"){
                query3.exec("UPDATE "+replaced+" SET setindex='time > 0 order by tot_faults asc' WHERE Id=1");
            }else if(setting->placingComboText=="According to the penalties and time"){
                query3.exec("UPDATE "+replaced+" SET setindex = 'time > 0 order by tot_faults asc, time asc' WHERE Id=1");
            }
        }else if(roundNum==2){
            if(setting->placingComboText == "According to the penalties"){
                query3.exec("UPDATE "+replaced+" SET setindex = '2time > 0 order by 2tot_faults asc' WHERE Id = 1");
            }else if(setting->placingComboText=="According to the penalties and time"){
                query3.exec("UPDATE "+replaced+" SET setindex = '2time > 0 order by 2tot_faults asc, 2tot_time asc' WHERE Id=1");
            }else if(setting->placingComboText=="Penalties in the 2nd Phase (penalties in the 1st Phase)"){
                query3.exec("UPDATE "+replaced+" SET setindex = '2time > 0 order by 2tot_faults asc, tot_faults asc' WHERE Id=1");
            }else if(setting->placingComboText=="Penalties and time in the 2nd Phase (penalties in the 1st Phase)"){
                query3.exec("UPDATE "+replaced+" SET setindex = '2time > 0 order by 2tot_faults asc, 2tot_time asc, tot_faults asc' WHERE Id=1");
            }else if(setting->placingComboText=="Penalties and time in the 2nd Phase (penalties and time in the 1st Phase)"){
                query3.exec("UPDATE "+replaced+" SET setindex = '2time > 0 order by 2tot_faults asc, 2tot_time asc, tot_faults asc, tot_time asc' WHERE Id=1");
                //TODO: to be done later
            }else if(setting->placingComboText=="Total time (Table C) of the 2nd Phase (penalties in the 1stPhase)"){
                //TODO: to be done later
            }
        }
    }
    ui->lstRiders->setModel(mModel);
    qDebug()<<ui->lstRiders->model()->rowCount();
}

void MainWindow::update_results() {
    if (firsttime == false) {
    QString v_brow = QString(c_country+c_place_code).toLower();
    if(roundNum==1){
        ui->results_webView->load (QUrl ("https://jumpemirates.com/assets/htmls/Jury1st/?var=" + v_brow +"event, 1"));
//        ui->results_webView->load (QUrl ("//localhost/eque/assets/htmls/Jury1st/?var=argcaeevent, 1"));
        qDebug() << "refresh 1 fase";
    }else if(roundNum==2){
        ui->results_webView->load (QUrl ("https://jumpemirates.com/assets/htmls/Jury2nd/?var=" + v_brow +"event, 1"));
        qDebug() << "refresh 2 fase";
    }else if(roundNum==3){
        ui->results_webView->load (QUrl ("https://jumpemirates.com/assets/htmls/Jury1jo/?var=" + v_brow +"event, 1"));
//        ui->results_webView->deleteLater();
        qDebug() << "refresh 1 joff";
    }
    qDebug()<<ui->lstRiders->model()->rowCount();
    if(ui->lstRiders->model()->rowCount() == 0){
        //NOTE: we show dialog here
        RoundSelectionDialog *dlg = new RoundSelectionDialog(this);
        connect(dlg, SIGNAL(changeRound(int)), this, SLOT(update_roundNum(int)));
        dlg->setWindowModality(Qt::ApplicationModal);
        dlg->show();
        dlg->raise();
        dlg->activateWindow();
    }
}
    firsttime = false;
}
void MainWindow::go_set(int index) {
    Q_UNUSED(index)
    SetEvent* setEventWindow = new SetEvent(ui->p_select->currentData());
    // When the save button is pressed in Set Event, update the corresponding components
    connect(setEventWindow, &SetEvent::saved, this, &MainWindow::update_event);

    setEventWindow->setWindowModality(Qt::ApplicationModal);
    setEventWindow->show();
    setEventWindow->raise();
    setEventWindow->activateWindow();
    if(roundNum==1){
        setEventWindow->select1stRound();
        jumpOffNum=0;
    }else if(roundNum==2){
        setEventWindow->select2ndRound();
    }else{
        setEventWindow->selectJumpOff();
        jumpOffNum++;
    }
}

void MainWindow::update_event(Setting* setting) {

    int index = ui->p_select->currentIndex();
    argEventQuery->seek(index);

    event_id = argEventQuery->value("event_id").toString().toLower();
    event_N = argEventQuery->value("event_N").toString();
    if (event_N.length() == 1) {
        event_N = "0" + event_N;
    }
    QString id = argEventQuery->value("Id").toString();
    QString strArticle = argEventQuery->value("article").toString();
    update_bd_obstacles(setting->obstacleData);
    update_obstacles(setting->obstacleData);
    set_table(setting);
    reset_counters();
    update_results();

    QSqlQuery event_set;
    QString sql = QString("SELECT against_clock FROM event_set WHERE article = '%1'").arg(strArticle);
    event_set.exec();

    if(event_set.next()) {
        int iAgainstClock = event_set.value("against_clock").toInt();
        mAgainstClock = (iAgainstClock == 1);
    }
    if(setting->p1stRound != NULL){
        mTimeAllow = setting->p1stRound->timeallowed;
        ui->timeAllowedSpin->setValue(mTimeAllow);
        mTimeLimit = setting->p1stRound->timelimit;
        mTableCompl = setting->p1stRound->cTableCompetition;
        QPushButton *button = ui->faults_widget->findChild<QPushButton *>("fourFaults");
        if(mTableCompl == 'A'){
            ui->tot_time->setVisible(false);
            button->setText("4 Faults");

        }
        else{
            ui->tot_time->setVisible(true);
            button->setText("6 Faults");
        }
    }
}

void MainWindow::update_bd_obstacles(QList<QList<int> > data) {
    // Variables to form BD table name
    QString lastColName = "no_order";
//    QSqlQuery query(DatabaseManager::database());
    QSqlQuery query;

    QString colsQueryStr = "";
    for (int i = 0; i < data.length(); i++) {
        for (int j = 0; j < data[i][0]; j++) {
            QString labelText = QString::number(i + 1);
            if (data[i][0] > 1) {
                if (j == 0) {
                    labelText.append("A");
                }
                else if (j == 1) {
                    labelText.append("B");
                }
                else if (j == 2) {
                    labelText.append("C");
                }
                else if (j == 3) {
                    labelText.append("D");
                }
            }
            // Create fault columns in BD
            if(roundNum==1){
                colsQueryStr += "ADD f" + labelText + " TINYINT(2) NOT NULL DEFAULT '0' AFTER " + lastColName + ", ";
                lastColName = "f" + labelText;
            }else if(roundNum==2){
                colsQueryStr += "ADD 2f" + labelText + " TINYINT(2) NOT NULL DEFAULT '0' AFTER " + lastColName + ", ";
                lastColName = "2f" + labelText;
            }else if(roundNum==3){
                colsQueryStr += "ADD "+QString::number(jumpOffNum) +"j"+ labelText + " TINYINT(2) NOT NULL DEFAULT '0' AFTER " + lastColName + ", ";
                lastColName = QString::number(jumpOffNum) +"j"+ labelText;
            }
        }
    }

    QString queryStr = "ALTER TABLE " + event_id + event_N + " " + colsQueryStr.left(colsQueryStr.size() - 2) + ";";
    query.exec(queryStr);

}

void MainWindow::update_obstacles(QList<QList<int> > data) {
    int buttonSize = 60;

    int maxCols = (int) floor(ui->faults_widget->width() / (buttonSize + ui->faults_widgetLayout->spacing()));
    int nElements = 0;
    int row;
    int col;

    qDeleteAll(obstaclesButtons);
    obstaclesButtons.clear();

    QString bColor = "#2bc19a";
    QString fColor = "#ff0000";
    for (int i = 0; i < data.length(); i++) {

        QString style =  "QPushButton {"
                         "    background-color: %1;"
                         "    color: %2;"
                         "    border: none;"
                         "}"
                         "QPushButton:!enabled {"
                         "    background-color: gray;"
                         "    color: %3;"
                         "}";

        if (data[i][0] == 1) {
            bColor = "#2bc19a";
            fColor = "#ffffff";
        }
        else if (data[i][0] == 2) {
            bColor = "#168165";
            fColor = "#ffffff";
        }
        else if (data[i][0] == 3) {
            bColor = "#164d3e";
            fColor = "#ffffff";
        }
        else if (data[i][0] == 4) {
            bColor = "green";
            fColor = "#ffffff";
        }

        for (int j = 0; j < data[i][0]; j++) {
            QWidget *obsWidget = new QWidget();
            QVBoxLayout *obsLayout = new QVBoxLayout();
            QPushButton *obsButton = new QPushButton();

            obsButton->setObjectName("obsButton" + QString::number(i) + "_" + QString::number(j));
            obsButton->resize(buttonSize, buttonSize);
            obsButton->setMinimumSize(QSize(buttonSize, buttonSize));
            obsButton->setMaximumSize(QSize(buttonSize, buttonSize));
            QString labelText = QString::number(i + 1);
            if (data[i][0] > 1) {
                if (j == 0) {
                    labelText.append("A");
                }
                else if (j == 1) {
                    labelText.append("B");
                }
                else if (j == 2) {
                    labelText.append("C");
                }
                else if (j == 3) {
                    labelText.append("D");
                }
            }

            obsButton->setText(labelText);
            obsButton->setFont(QFont("Sans Serif", 14));

            obsLayout->addWidget(obsButton);
            obsLayout->setMargin(0);
            obsWidget->setStyleSheet(style.arg(bColor).arg(fColor).arg(fColor));
            obsWidget->setLayout(obsLayout);

            row = (int) floor(nElements / maxCols);
            col = nElements % maxCols;

            obstaclesButtons.append(obsWidget);
            ui->faults_widgetLayout->addWidget(obsWidget, row, col, Qt::AlignLeft);
            connect(obsButton, &QPushButton::pressed, this, &MainWindow::update_faults);

            nElements += 1;
        }

    }
    QPushButton *fourFaults = ui->faults_widget->findChild<QPushButton*>("fourFaults");
    if(fourFaults==0){
        fourFaults=new QPushButton();
        fourFaults->setObjectName("fourFaults");
        fourFaults->setFixedSize(QSize(90, 90));
        fourFaults->setParent(ui->faults_widget);
        ui->faults_widgetLayout->addWidget(fourFaults, row+1, maxCols-1, Qt::AlignRight);
        connect(fourFaults, &QPushButton::clicked, this, &MainWindow::fourFaults_clicked);
    }else{
        ui->faults_widgetLayout->removeWidget(fourFaults);
        ui->faults_widgetLayout->addWidget(fourFaults, row+1, maxCols-1, Qt::AlignRight);
    }
    row = (int) floor(nElements / maxCols);
    col = nElements % maxCols;
}

void MainWindow::reset_obstacles() {
    for (int i = 0; i < obstaclesButtons.length(); i++) {
        obstaclesButtons[i]->findChildren<QPushButton*>()[0]->setEnabled(true);
    }
}

void MainWindow::update_faults() {
    if (state == COMP_START) {
        QPushButton* button = (QPushButton*) sender();
        button->setEnabled(false);

        int currentFaults = ui->faults->intValue();
        ui->faults->display(currentFaults + 4);
        int currentTotFaults =ui->tot_faults->intValue();
        ui->tot_faults->display(currentTotFaults + 4);
        mFaultAsTime += 6;

        int currentTotTime = floor(ui->elapsed_time->value());
        ui->tot_time->display(currentTotTime + mFaultAsTime);
    }
}

void MainWindow::update_countdown() {
    mElapseTime += 0.003;

    QString strTime = QString::number(mElapseTime, 'f', 1);

    ui->elapsed_time->display(strTime);
    if(mElapseTime >= 0.0){
        double totaltime = mElapseTime + mFaultAsTime;
        strTime = QString::number(totaltime, 'f', 1);
        ui->tot_time->display(strTime);

        if(mElapseTime >= mTimeAllow){
            if(mAgainstClock == true){
                int penalty =  mElapseTime - mTimeAllow;
                penalty += 1;
                ui->penalty->display(penalty);
            }
            else{
                int time_exceeded =  mElapseTime - mTimeAllow;
                int penalty = time_exceeded / 4.0;
                penalty += 1;
                if(penalty > 0)
                {
                    ui->penalty->display(penalty);
                }
            }
            int faults = ui->faults->value();
            int penalty = ui->penalty->value();
            ui->tot_faults->display(faults + penalty);
        }
        if(state == COMP_GO){
            state = COMP_RESUME;
        }
    }
}
void MainWindow::playMp3(QString strQResourceFile)
{
    mMediaPlayer.stop();
    mMediaPlayer.setMedia(QUrl(strQResourceFile));
    mMediaPlayer.play();
}

void MainWindow::update_roundNum(int round)
{
    roundNum = round;
    if(round !=1){
        go_set(0);
    }

}

void MainWindow::start_recording()
{

}

void MainWindow::stop_recording()
{

}


/*
void delay()
{
    QTime dieTime= QTime::currentTime().addSecs(1);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}
para crear un delay que no afecta al GUI
*/

QString MainWindow::rename_video() {
#ifdef _WIN32
    QString oldvideoName = "C:/tarola/history/riderout.mp4";
    QString save_folder = "C:/tarola/history/";
#elif linux
    QString save_folder = "/home/maek/obs_videos/";
    QString oldvideoName = "/home/maek/riderout.flv";
#endif

    std::this_thread::sleep_for(std::chrono::seconds{2});
    QString newvideoName = QString::number(QDateTime::currentMSecsSinceEpoch()) + ".mp4";

    QFile f(oldvideoName);
    // Agregas la carpeta + nuevo nombre de archivo.
    if (f.rename(save_folder + newvideoName)) {
#ifdef _WIN32
        m_file = new QFile(save_folder + newvideoName);
#elif linux
        m_file = new QFile(save_folder + newvideoName);
#endif
        QFileInfo fileInfo(*m_file);

        QUrl url("ftp://jumpemirates.com/assets/history/" + newvideoName);
        url.setUserName("freelan@jumpemirates.com");
        url.setPassword("freelan");
        url.setPort(21);

        if (m_file->open(QIODevice::ReadOnly)) {
            // upload
            m_manager = new QNetworkAccessManager(this);
            QNetworkReply *reply = m_manager->put(QNetworkRequest(url), m_file);
            connect(m_manager, SIGNAL(finished(QNetworkReply *)), this,
                    SLOT(replyFinished(QNetworkReply *)));
            onupload = onupload + 1;

            qDebug() << reply->error();
        }
    } else {
        qDebug() << f.errorString();
        qWarning("Unable to rename video file.");
        return "";
    }
    return newvideoName;
}

void MainWindow::replyFinished(QNetworkReply *reply) {
    if (reply->error()) {
        qDebug() << "ERROR!";
        qDebug() << reply->errorString();
    } else {
        onupload = onupload - 1;
        qDebug() << "DO Something!";
        if (onupload == 0) {
            m_file->close();
            m_file->deleteLater();
            qDebug() << "freeup video" << onupload;
            //            send_video_fb();
        }
        //          ui->send_webView->load
        //          (QUrl("https://jumpemirates.com/dos/?var="+(c_country+"history").toLower()));
        qDebug() << "video" << onupload;
        qDebug() << "https://jumpemirates.com/dos/?var=" +
                        (c_country + "history").toLower();
        reply->deleteLater();
    }
}

void MainWindow::send_video_fb()
{
//    ui->send_webView->load (QUrl("https://jumpemirates.com/dos/?var="+c_country+"history"));
}

void MainWindow::on_go_button_pressed()
{
    if (state == INITIAL) {
        ui->start_button->setVisible(true);
        ui->start_button->show();

        ui->start_button->setEnabled(true);
        bs = 1; //Start barrier now ready

        //pasar datos para html
//        QSqlQuery query(DatabaseManager::database());
        QSqlQuery query;
        QString inpos = mModel->data(mModel->index(0, mModel->fieldIndex("in_pos"))).toString();
        QString replaced = QString(c_country+c_place_code+"event").toLower();

        query.exec("UPDATE "+replaced+", " + event_id + event_N + " SET rider=" + event_id + event_N + ".Name, horse=" + event_id + event_N + ".name_H, "+replaced+".rider_img=" + event_id + event_N + ".rider_img, "+replaced+".horse_img=" + event_id + event_N + ".horse_img, "+replaced+".ing=" + event_id + event_N + ".in_pos, "+replaced+".height= height WHERE "+replaced+".Id = 1 AND " + event_id + event_N + ".in_pos = '"+inpos+"'");
//        query.finish();
        state = COMP_GO;
        bBelRingging = true;

        playMp3("qrc:/sounds/res/Ring.mp3");
    }
    else if (state == COMP_GO){
        if(mMediaPlayer.state() == QMediaPlayer::PlayingState && bBelRingging == true){
            mMediaPlayer.pause();
        }
        timer->stop();
//        bs = 0; // desable barriers
        state = COMP_PAUSE;
    }
    else if (state == COMP_PAUSE) {
        if(mElapseTime < 0.0){
            if(mMediaPlayer.state() == QMediaPlayer::PausedState && bBelRingging == true){
                mMediaPlayer.play();
            }
            state = COMP_GO;
        }
        else{
            state = COMP_RESUME;
        }
        if(bBelRingging == false)
        {
            bs = 2; // after resume round end barrier set ready
            timer->start();
        }
    }
    else if (state == COMP_RESUME) {
        timer->stop();
        state = COMP_PAUSE;
    }
    update_go_button_title();
}

void MainWindow::on_start_button_clicked()
{
    if (state == COMP_START) {
        on_end_button_clicked();
        return;
    }
    else if (state == COMP_PAUSE) {
        if(mMediaPlayer.state() == QMediaPlayer::PlayingState && bBelRingging == true){
            mMediaPlayer.stop();
        }
        timer->start();
		emit ir_start();
    }
    else if (state == COMP_GO && mMediaPlayer.state() == QMediaPlayer::PlayingState && bBelRingging == true){
        mMediaPlayer.stop();
        timer->start();
		emit ir_start();
    }

    if(mElapseTime < 0) {
        mElapseTime = 0.0;
        ui->elapsed_time->display("0.0");
    }

    ui->go_button->setEnabled(false);
    ui->pause->setEnabled(true);
    state = COMP_START;
    update_go_button_title();

    emit ir_start();

//    start_recording();
}

void MainWindow::on_end_button_clicked()
{
    timer->stop();
	emit ir_end();
//    stop_recording();
    bs = 0;  //reset barriers

    ConfirmEndRouteDlg dlg;
    dlg.setValues(QString::number(ui->faults->value()), QString::number(mElapseTime, 'f', 3), QString::number(ui->penalty->value()), QString::number(ui->tot_faults->value()));

    int result = dlg.exec();

    if(result == QMessageBox::Rejected)
    {
        reset_obstacles();
        reset_counters();

        state = INITIAL;
        return;
    }



    QString video_name=rename_video();
    qDebug() << video_name;

    QString Id_row,rider_name,horse_name,nf,m_id;
//    Id_row=mModel->data(mModel->index(0,mModel->fieldIndex("Id"))).toString();
    rider_name=mModel->data(mModel->index(0,mModel->fieldIndex("Name"))).toString();
    horse_name=mModel->data(mModel->index(0,mModel->fieldIndex("name_H"))).toString();
    nf=mModel->data(mModel->index(0,mModel->fieldIndex("NF"))).toString();
    m_id=mModel->data(mModel->index(0,mModel->fieldIndex("messenger"))).toString();
    if(roundNum==1){
        mModel->setData(mModel->index(0, mModel->fieldIndex("fault")),
                        dlg.getFaultsValue());
        mModel->setData(mModel->index(0, mModel->fieldIndex("time")),
                        dlg.getTimeValue());
        mModel->setData(mModel->index(0, mModel->fieldIndex("tot_faults")),
                        dlg.getTotalFaultsValue());
        mModel->setData(mModel->index(0, mModel->fieldIndex("penalty")),
                        dlg.getPenaltyValue());
        mModel->setData (mModel-> index (0, mModel-> fieldIndex ("tot_time")),
                         dlg.getTimeValue ());
    }else if(roundNum==2){
        mModel->setData(mModel->index(0, mModel->fieldIndex("2fault")),
                        dlg.getFaultsValue());
        mModel->setData(mModel->index(0, mModel->fieldIndex("2time")),
                        dlg.getTimeValue());
        mModel->setData(mModel->index(0, mModel->fieldIndex("2tot_faults")),
                        dlg.getTotalFaultsValue());
        mModel->setData(mModel->index(0, mModel->fieldIndex("2penalty")),
                        dlg.getPenaltyValue());
        mModel->setData (mModel-> index (0, mModel-> fieldIndex ("2tot_time")),
                         dlg.getTimeValue ());
    }else if(roundNum==3){
        mModel->setData(mModel->index(0, mModel->fieldIndex("jtime")),
                        dlg.getTimeValue());
        mModel->setData(mModel->index(0, mModel->fieldIndex("jfault")),
                        dlg.getTotalFaultsValue());
    }
    // Update faults on BD
    for (int i = 0; i < obstaclesButtons.length(); i++) {
        if (!obstaclesButtons[i]->findChildren<QPushButton*>()[0]->isEnabled()) {
            mModel->setData(mModel->index(0, mModel->fieldIndex("f" + obstaclesButtons[i]->findChildren<QPushButton*>()[0]->text())), "4");
        }
    }
//    mModel->database().transaction();
    mModel->submitAll();  //update table
    qDebug() << "Row " << mModel->rowCount(QModelIndex());
    if (!mModel->submitAll()) {
//        set_table();
        qWarning() << "Error saving data.  Error is: " << mModel->lastError();
//        ui->lstRiders->setModel(mModel);
        mModel->database().rollback();
        mModel->submitAll();
        if (!mModel->submitAll()) {
            qWarning() << "Second Error saving data.  Error is: " << mModel->lastError();
            QSqlQuery query(DatabaseManager::database());
            qDebug() << Id_row;
            query.exec("UPDATE argcae2019_02_2601  SET fault = 9 where Id = "+Id_row);
            if( !query.exec())
                    qDebug() << "Fail to update" << query.lastError();
        }
    }
        qDebug() << "model submited " << rider_name;

        QSqlQuery query(DatabaseManager::database());
        QString v_history = QString(c_country+"history").toLower();
        query.exec("INSERT INTO "+v_history+" (Name, name_H, nf, place_code, date, video, m_id) VALUES ('"+
                   rider_name+"', '"+horse_name+"', '"+nf+"', '"+c_place_code+"', CURDATE(), '"+video_name+"', '"+m_id+"');");

        mModel->select();

    update_results();
    reset_obstacles();
    reset_counters();
    state = INITIAL;
}

void MainWindow::update_go_button_title() {
    QString titleForGoBtn;
    QString titleForStartBtn = "START";
    switch (state) {
    case COMP_PAUSE:
        titleForGoBtn = "RESUME";
        break;
    case COMP_GO:
    case COMP_RESUME:
        titleForGoBtn = "PAUSE";
        break;
    case COMP_START:
        titleForGoBtn = "GO";
        titleForStartBtn = "END";
        break;
    default:
        titleForGoBtn = "GO";
        break;
    }
    if(state == COMP_START){
        ui->go_button->setVisible(false);
    }
    else{
        ui->go_button->setVisible(true);
    }
    ui->go_button->setText(titleForGoBtn);
    ui->start_button->setText(titleForStartBtn);
}

void MainWindow::resetAll() {
    state = INITIAL;
    timer->stop();
    ui->go_button->setEnabled(true);
    update_results();
    reset_obstacles();
    reset_counters();
    update_go_button_title();
    bs = 0;
}

void MainWindow::on_bell_clicked()
{
    QSound::play(":/sounds/res/bell.wav");
}

void MainWindow::on_pause_clicked()
{
    if (ui->pause->text() == "Pause") {
        ui->pause->setText("Continue");
        timer->stop();
    } else {
        ui->pause->setText("Pause");
        timer->start();
    }
}

void MainWindow::on_retired_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::warning(this, "", "    Is retired?    ",
                                 QMessageBox::Yes|QMessageBox::No);
    if(reply == QMessageBox::Yes){
        timer->stop();
        bs = 0;  //reset barriers
        QString video_name=rename_video();
        if(roundNum==1){
            mModel->setData(mModel->index(0, mModel->fieldIndex("tot_faults")),
                            88.88);
            mModel->setData(mModel->index(0, mModel->fieldIndex("time")),
                            8888);
            mModel->setData(mModel->index(0, mModel->fieldIndex("tot_time")),
                            8888);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2time")),
                            8888);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2faults")),
                            88.88);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jtime")),
                            8888);

        }else if(roundNum==2){
            mModel->setData(mModel->index(0, mModel->fieldIndex("2faults")),
                            88.88);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2tot_faults")),
                            88.88);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jfault")),
                            88.88);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2time")),
                            8888);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2tot_time")),
                            8888);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jtime")),
                            8888);
        }else if(roundNum==3){
            mModel->setData(mModel->index(0, mModel->fieldIndex("jfault")),
                            88.88);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jtime")),
                            8888);
        }else if(roundNum==4){
            mModel->setData(mModel->index(0, mModel->fieldIndex("jfault")),
                            88.88);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jtime")),
                            8888);
        }
        mModel->submitAll();
        mModel->select();

        resetAll();
    }
}

void MainWindow::on_eliminated_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::warning(this, "", "    Is eliminated?    ",
                                 QMessageBox::Yes|QMessageBox::No);
    if(reply == QMessageBox::Yes){
        timer->stop();
        bs = 0;  //reset barriers
        QString video_name=rename_video();
        if(roundNum==1){
            mModel->setData(mModel->index(0, mModel->fieldIndex("tot_faults")),
                            77.77);
            mModel->setData(mModel->index(0, mModel->fieldIndex("time")),
                            7777);
            mModel->setData(mModel->index(0, mModel->fieldIndex("tot_time")),
                            7777);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2time")),
                            7777);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2faults")),
                            77.77);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jtime")),
                            7777);

        }else if(roundNum==2){
            mModel->setData(mModel->index(0, mModel->fieldIndex("2faults")),
                            77.77);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2tot_faults")),
                            77.77);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jfault")),
                            77.77);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2time")),
                            7777);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2tot_time")),
                            7777);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jtime")),
                            7777);
        }else if(roundNum==3){
            mModel->setData(mModel->index(0, mModel->fieldIndex("jfault")),
                            77.77);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jtime")),
                            7777);
        }else if(roundNum==4){
            mModel->setData(mModel->index(0, mModel->fieldIndex("jfault")),
                            77.77);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jtime")),
                            7777);
        }
        mModel->submitAll();
        mModel->select();

        resetAll();
    }
}

void MainWindow::on_not_participate_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::warning(this, "", "    Not participate?    ",
                                 QMessageBox::Yes|QMessageBox::No);
    if(reply == QMessageBox::Yes){

        if(roundNum==1){
            mModel->setData(mModel->index(0, mModel->fieldIndex("tot_faults")),
                            99.99);
            mModel->setData(mModel->index(0, mModel->fieldIndex("time")),
                            9999);
            mModel->setData(mModel->index(0, mModel->fieldIndex("tot_time")),
                            9999);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2time")),
                            9999);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2faults")),
                            99.99);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jtime")),
                            9999);

        }else if(roundNum==2){
            mModel->setData(mModel->index(0, mModel->fieldIndex("2faults")),
                            99.99);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2tot_faults")),
                            99.99);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jfault")),
                            99.99);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2time")),
                            9999);
            mModel->setData(mModel->index(0, mModel->fieldIndex("2tot_time")),
                            9999);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jtime")),
                            9999);
        }else if(roundNum==3){
            mModel->setData(mModel->index(0, mModel->fieldIndex("jfault")),
                            99.99);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jtime")),
                            9999);
        }else if(roundNum==4){
            mModel->setData(mModel->index(0, mModel->fieldIndex("jfault")),
                            99.99);
            mModel->setData(mModel->index(0, mModel->fieldIndex("jtime")),
                            9999);
        }
        mModel->submitAll();
        mModel->select();

        resetAll();
    }
}

void MainWindow::on_mediaPlayer_mediaStatusChanged(QMediaPlayer::MediaStatus status)
{
    if(bBelRingging == true && status == QMediaPlayer::EndOfMedia )
    {
        bBelRingging = false;
        timer->start();
    }
}

void MainWindow::on_timeAllowedSpin_valueChanged(double arg1)
{
    mTimeAllow=arg1;
}

void MainWindow::fourFaults_clicked()
{
    if (state == COMP_START) {
    int faults=4;
    if(mTableCompl =='C'){
        faults=6;
    }
    int currentFaults = ui->faults->intValue();
    ui->faults->display(currentFaults + faults);
    int currentTotFaults =ui->tot_faults->intValue();
    ui->tot_faults->display(currentTotFaults + faults);
}
}
