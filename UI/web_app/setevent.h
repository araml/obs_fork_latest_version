#ifndef SETEVENT_H
#define SETEVENT_H

#include <QWidget>
#include <QLineEdit>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QRadioButton>
#include <QCheckBox>
#include <QComboBox>
#include <QLabel>
#include <QDebug>
#include <QList>
class RoundSetting{
public:
    double distance;
    double timeallowed;
    double timelimit;
    char   cTableCompetition;
};
class Setting{
public:
    QString strArticle;
    RoundSetting* p1stRound;
    RoundSetting* p2ndRound;
    QList<QList<int>> obstacleData;
    QString startComboText;
    bool faultCheck;
    bool riderCheck;
    bool percentCheck;
    int faultNum;
    int riderNum;
    int percentNum;
    QString placingComboText;
};
namespace Ui {
class SetEvent;
}

class SetEvent : public QWidget
{
    Q_OBJECT

public:
    explicit SetEvent(QVariant narticle, QWidget *parent = 0);
    ~SetEvent();
public slots:
    void select1stRound();
    void select2ndRound();
    void selectJumpOff();
    void allowButton();

signals:
    void saved(Setting*);

private:
    Ui::SetEvent *ui;
    QList<QList <QWidget*>> obstaclesList;
//    void updateJoTooVisibility(bool jo_too_visible, bool jo_too_2_visible);
    void updateWidgetVisibility(QWidget *widget, bool visibility);
    int roundNum=1;

private slots:
    void updateObstacles(const QString &text);
    void updateTimes(QComboBox *speed_combo, QLineEdit *distance_line, QLineEdit *timeA_line, QLineEdit *timeL_line);
    void on_pushButton_clicked();
    void on_art245_clicked();
    void on_art274_clicked();
    void on_art274_2_clicked();
    void on_clock_toggled(bool checked);
    void on_clock_2_toggled(bool checked);
    void on_checkBox_toggled(bool checked);
    void on_checkBox_2_toggled(bool checked);
    void on_checkBox_3_toggled(bool checked);
    void on_r1Table_combo_activated(const QString &arg1);
//    void on_r1Distance_line_textChanged(const QString &arg1);
    void on_r2Table_combo_activated(const QString &arg1);
};

#endif // SETEVENT_H
