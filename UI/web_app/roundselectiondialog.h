#ifndef ROUNDSELECTIONDIALOG_H
#define ROUNDSELECTIONDIALOG_H

#include <QDialog>

namespace Ui {
class RoundSelectionDialog;
}

class RoundSelectionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RoundSelectionDialog(QWidget *parent = 0);
    ~RoundSelectionDialog();

signals:
    void changeRound(int value);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::RoundSelectionDialog *ui;
};

#endif // ROUNDSELECTIONDIALOG_H
