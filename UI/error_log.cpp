#include "error_log.h"
#include <cstdio>
#include <thread>
#include <mutex>
#include <stdarg.h>


using namespace std;

static mutex file_writer;
static FILE *file_handler = fopen("log_PDE.txt", "w");

void log_pde(const char *format, ...) {
    unique_lock<mutex> lk(file_writer);
    char out[4096];
    va_list args;
    va_start(args, format);
    vsnprintf(out, sizeof(out), format, args);
    fprintf(file_handler, "LOG: %s", out);
    fflush(file_handler);
    va_end(args);
}
